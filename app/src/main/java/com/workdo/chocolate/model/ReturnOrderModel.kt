package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class ReturnOrderModel(

	@field:SerializedName("data")
	val data: ReturnOrderData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null

)

data class ReturnOrderItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("demo_field")
	val demoField: String? = null,

	@field:SerializedName("amount")
	val amount: Double? = null,

	@field:SerializedName("product_order_id")
	val productOrderId: String? = null,

	@field:SerializedName("delivered_status")
	val deliveredStatus: Int? = null,

	@field:SerializedName("delivered_image")
	val deliveredImage: String? = null,

	@field:SerializedName("order_id_string")
	val orderIdString: String? = null,

	@field:SerializedName("delivery_id")
	val deliveryId: Int? = null,

	@field:SerializedName("delivered_status_string")
	val deliveredStatusString: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("return_date")
	val returnDate: String? = null
)

data class ReturnOrderData(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<ReturnOrderItem>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: Any? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("links")
	val links: ArrayList<LinksItem>? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)
