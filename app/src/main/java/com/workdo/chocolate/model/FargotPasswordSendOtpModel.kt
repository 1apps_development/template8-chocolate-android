package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class FargotPasswordSendOtpModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

