package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class CommonModel(
    @field:SerializedName("count")
    val count: String? = null,
    @field:SerializedName("data")
    val data: CommonData? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class CommonData(
    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("currency")
    val currency: String? = null,

    @field:SerializedName("currency_name")
    val currency_name: String? = null,

    @field:SerializedName("terms")
    val terms: String? = null,

    @field:SerializedName("insta")
    val insta: String? = null,

    @field:SerializedName("youtube")
    val youtube: String? = null,

    @field:SerializedName("messanger")
    val messanger: String? = null,

    @field:SerializedName("twitter")
    val twitter: String? = null,

    @field:SerializedName("return_policy")
    val returnPolicy: String? = null,

    @field:SerializedName("point")
    val point: String? = null,

    @field:SerializedName("contact_us")
    val contactUs: String? = null
)
