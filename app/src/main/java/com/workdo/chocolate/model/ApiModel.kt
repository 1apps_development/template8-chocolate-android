package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class ApiModel(

    @field:SerializedName("data")
    val data: ApiData? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ApiData(
    @field:SerializedName("base_url")
    val base_url: String? = null,

    @field:SerializedName("image_url")
    val image_url: String? = null,

    @field:SerializedName("payment_url")
    val payment_url: String? = null,
)