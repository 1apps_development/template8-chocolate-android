package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class ProductBannerModel(

	@field:SerializedName("data")
	val data: Datas? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class ProductsHeaderBanners(

	@field:SerializedName("products-header-banner-title-text")
	val productsHeaderBannerTitleText: String? = null,

	@field:SerializedName("products-header-banner")
	val productsHeaderBanner: String? = null
)

data class ThemJsons(

	@field:SerializedName("products-header-banner")
	val productsHeaderBanner: ProductsHeaderBanners? = null
)

data class Datas(

	@field:SerializedName("them_json")
	val themJson: ThemJsons? = null
)
