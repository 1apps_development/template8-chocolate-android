package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class OrderModel(

	@field:SerializedName("data")
	val data: OrderTextData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class OrderTextData(

	@field:SerializedName("complete_order")
	val completeOrder: CompleteOrder? = null,
	@field:SerializedName("message")
	val message: String? = null,
	@field:SerializedName("order_id")
	val orderId: Int? = null
)

data class CompleteOrder(

	@field:SerializedName("order-complate")
	val orderComplate: OrderComplate? = null
)

data class OrderComplate(

	@field:SerializedName("order-complate-title")
	val orderComplateTitle: String? = null,
	@field:SerializedName("message")
	val message: String? = null,
	@field:SerializedName("order-complate-description")
	val orderComplateDescription: String? = null
)
