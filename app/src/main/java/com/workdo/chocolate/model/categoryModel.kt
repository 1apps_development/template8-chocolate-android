package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName



data class CategoryModel(

	@field:SerializedName("data")
	val data: ArrayList<CategoryDataItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CategoryDataItem(

	var isSelect: Boolean = false,

	@field:SerializedName("trending")
	val trending: String? = null,

	@field:SerializedName("product_count")
	val productCount: String? = null,

	@field:SerializedName("theme_id")
	val themeId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("demo_field")
	val demoField: String? = null,

	@field:SerializedName("maincategory_id")
	val maincategoryId: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("image_path")
	val imagePath: String? = null,

	@field:SerializedName("icon_path")
	val iconPath: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("category_item")
	val categoryItem: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

