package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class MenulistModel(

	@field:SerializedName("data")
	val data: ArrayList<DataItems>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class DataItems(
	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("icon_img_path")
	val iconImgPath: String? = null,

	@field:SerializedName("sub_category")
	val subCategory: List<SubCategoryItems>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("theme_id")
	val themeId: String? = null,

	var expand: Boolean = false,

	@field:SerializedName("id")
	val id: Int? = null

)

data class SubCategoryItems(

	@field:SerializedName("subcategory_id")
	val subcategoryId: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("maincategory_id")
	val maincategoryId: Int? = null,

	@field:SerializedName("icon_img_path")
	val iconImgPath: String? = null,

	@field:SerializedName("name")
	val name: String? = null
)
