package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class VariantStockResponse(

	@field:SerializedName("data")
	val data: StockData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class StockData(

	@field:SerializedName("original_price")
	val originalPrice: String? = null,

	@field:SerializedName("final_price")
	val finalPrice: String? = null,

	@field:SerializedName("discount_price")
	val discountPrice: String? = null,

	@field:SerializedName("variant")
	val variant: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("stock")
	val stock: Int? = null
)
