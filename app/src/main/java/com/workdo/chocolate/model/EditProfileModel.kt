package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class EditProfileModel(

	@field:SerializedName("data")
	val data: EditProfile? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class EditProfile(

	@field:SerializedName("data")
	val data: Data? = null,

)
