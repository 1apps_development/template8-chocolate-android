package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName


data class ConfirmModel(

	@field:SerializedName("payment_type")
	var paymentType: String? = null,

	@field:SerializedName("billing_info")
	var billingInfo: BillingInfoPost? = null,

	@field:SerializedName("user_id")
	var userId: Int? = null,

	@field:SerializedName("delivery_id")
	var deliveryId: Int? = null,

	@field:SerializedName("payment_comment")
	var paymentComment: String? = null,

	@field:SerializedName("delivery_comment")
	var deliveryComment: String? = null,

	@field:SerializedName("coupon_info")
	var couponInfo: CouponInfoPost? = null,
	@field:SerializedName("theme_id")
	var themeId: String? = null
)

data class CouponInfoPost(

	@field:SerializedName("coupon_discount_type")
	var couponDiscountType: String? = null,

	@field:SerializedName("coupon_code")
	var couponCode: String? = null,

	@field:SerializedName("coupon_id")
	var couponId: String? = null,

	@field:SerializedName("coupon_discount_amount")
	var couponDiscountAmount: String? = null,

	@field:SerializedName("coupon_discount_number")
	var couponDiscountNumber: String? = null,

	@field:SerializedName("coupon_final_amount")
	var couponFinalAmount: String? = null,

	@field:SerializedName("coupon_name")
	var couponName: String? = null,

	@field:SerializedName("theme_id")
var themeId: String? = null
)

data class BillingInfoPost(

	@field:SerializedName("delivery_country")
	var deliveryCountry: Int? = null,

	@field:SerializedName("firstname")
	var firstname: String? = null,

	@field:SerializedName("delivery_address")
	var deliveryAddress: String? = null,

	@field:SerializedName("billing_user_telephone")
	var billingUserTelephone: String? = null,

	@field:SerializedName("billing_country")
	var billingCountry: Int? = null,

	@field:SerializedName("billing_address")
	var billingAddress: String? = null,

	@field:SerializedName("billing_postecode")
	var billingPostecode: String? = null,

	@field:SerializedName("delivery_state")
	var deliveryState: Int? = null,

	@field:SerializedName("billing_company_name")
	var billingCompanyName: String? = null,

	@field:SerializedName("lastname")
	var lastname: String? = null,

	@field:SerializedName("billing_city")
	var billingCity: String? = null,

	@field:SerializedName("billing_state")
	var billingState: Int? = null,

	@field:SerializedName("delivery_postcode")
	var deliveryPostcode: String? = null,

	@field:SerializedName("delivery_city")
	var deliveryCity: String? = null,

	@field:SerializedName("email")
	var email: String? = null
)
