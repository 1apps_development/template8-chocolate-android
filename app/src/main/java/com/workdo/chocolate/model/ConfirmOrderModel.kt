package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class ConfirmOrderModel(

	@field:SerializedName("data")
	val data: ConfirmOrderData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class ConfirmOrderData(

	@field:SerializedName("billing_information")
	val billingInformation: BillingInformation? = null,

	@field:SerializedName("delivery")
	val delivery: String? = null,

	@field:SerializedName("message")
	val message: String? = null,


	@field:SerializedName("product")
	val product: ArrayList<ProductListItem>? = null,

	@field:SerializedName("delivery_charge")
	val deliveryCharge: String? = null,

	@field:SerializedName("coupon")
	val coupon: Coupon? = null,

	@field:SerializedName("paymnet")
	val paymnet: String? = null,

	@field:SerializedName("final_price")
	val finalPrice: String? = null,

	@field:SerializedName("subtotal")
	val subtotal: String? = null,

	@field:SerializedName("delivery_information")
	val deliveryInformation: DeliveryInformation? = null,

	@field:SerializedName("tax")
	val tax: ArrayList<TaxItem>? = null,

	@field:SerializedName("coupon_info")
	val couponInfo: CouponData? = null
)

data class CouponorderInfo(

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("discount_string")
	val discountString: String? = null,

	@field:SerializedName("price")
	val price: String? = null,

	@field:SerializedName("discount_amount")
	val discountAmount: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("discount_string2")
	val discountString2: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Boolean? = null

)

data class BillingInformation(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("postecode")
	val postecode: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class Coupon(

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("discount_string")
	val discountString: String? = null,

	@field:SerializedName("price")
	val price: String? = null
)


data class DeliveryInformation(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("postecode")
	val postecode: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
