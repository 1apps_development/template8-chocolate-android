package com.workdo.chocolate.model

data class BillingDetailsAddress(
    val billing_title:String,

    val billing_address: String,
    val billing_city: String,
    val billing_postecode: String,
    val billing_country: String,
    val billing_state: String,
    val billingCountryName:String,
    val billingStateName:String,
    val billing_user_telephone: String,
    val delivery_address: String,
    val delivery_city: String,
    val delivery_country: String,
    val delivery_postcode: String,
    val delivery_state: String,
    val deliveryCountryName:String,
    val deliveryStateName:String,
    val email: String,
    val firstname: String,
    val lastname: String
)
