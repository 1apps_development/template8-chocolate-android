package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class CountryModel(

	@field:SerializedName("data")
	val data: ArrayList<CountryDataItem>? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CountryDataItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
