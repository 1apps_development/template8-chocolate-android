package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class OrderDetailsModel(

    @field:SerializedName("data")
    val data: OrderDetailsData? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ProductItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("return")
    val returnStatus: String? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("variant_id")
    val variantId: Int? = null,

    @field:SerializedName("final_price")
    val finalPrice: String? = null,

    @field:SerializedName("discount_price")
    val discountPrice: String? = null,

    @field:SerializedName("product_id")
    val productId: Int? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("per_product_discount_price")
    val perProductDiscountPrice: String? = null,

    @field:SerializedName("total_orignal_price")
    val totalOrignalPrice: String? = null,

    @field:SerializedName("variant_name")
    val variantName: String? = null,

    @field:SerializedName("orignal_price")
    val orignalPrice: String? = null
)

data class DeliveryInformations(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("post_code")
    val postCode: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("email")
    val email: String? = null
)

data class CouponInfo(

    @field:SerializedName("code")
    val code: String? = null,

    @field:SerializedName("discount_string")
    val discountString: String? = null,
    @field:SerializedName("discount_string2")
    val discountString2: String? = null,

    @field:SerializedName("price")
    val price: String? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Boolean? = null

)

data class TaxItem(

    @field:SerializedName("amountstring")
    val amountstring: String? = null,

    @field:SerializedName("tax_string")
    val taxString: String? = null,

    @field:SerializedName("tax_price")
    val taxPrice: String? = null,
    @field:SerializedName("tax_amount")
    val tax_amount: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("tax_type")
    val tax_type: String? = null,
    @field:SerializedName("tax_name")
    val tax_name: String? = null,

    @field:SerializedName("tax_id")
    val taxId: Int? = null
)

data class BillingInformations(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("post_code")
    val postCode: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("email")
    val email: String? = null
)

data class OrderDetailsData(

    @field:SerializedName("billing_informations")
    val billingInformations: BillingInformations? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("delivery")
    val delivery: String? = null,

    @field:SerializedName("product")
    val product: ArrayList<ProductItem>? = null,

    @field:SerializedName("paymnet")
    val paymnet: String? = null,

    @field:SerializedName("final_price")
    val finalPrice: String? = null,

    @field:SerializedName("delivery_informations")
    val deliveryInformations: DeliveryInformations? = null,

    @field:SerializedName("delivered_charge")
    val deliveredCharge: String? = null,

    @field:SerializedName("sub_total")
    val subTotal: String? = null,
    @field:SerializedName("is_review")
    val is_review: Int? = null,

    @field:SerializedName("tax")
    val tax: ArrayList<TaxItem>? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("order_status")
    val orderStatus: Int? = null,

    @field:SerializedName("order_id")
    val orderId: String? = null,

    @field:SerializedName("order_status_message")
    val orderStatusMessage: String? = null,
    @field:SerializedName("return_status_message")
    val returnStatusMessage: String? = null,

    @field:SerializedName("return_status")
    val returnStatus: Boolean? = null,

    @field:SerializedName("coupon_info")
    val couponInfo: CouponInfo? = null
)
