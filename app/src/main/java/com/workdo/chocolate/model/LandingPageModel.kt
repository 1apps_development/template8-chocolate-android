package com.workdo.chocolate.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header")
	val homepageHeader: String? = null,

	@field:SerializedName("homepage-header-bg-img")
	val homepageHeaderBgImg: String? = null,

	@field:SerializedName("homepage-header-heading")
	val homepageHeaderHeading: String? = null,

	@field:SerializedName("homepage-header-sub-text")
	val homepageHeaderSubText: String? = null,

	@field:SerializedName("homepage-header-title-text")
	val homepageHeaderTitleText: String? = null
)

data class LandingPageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,
	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)

data class HomepageNewsletter(

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-description")
	val homepageNewsletterDescription: String? = null,

	@field:SerializedName("homepage-newsletter-title-text")
	val homepageNewsletterTitleText: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-newsletter")
	val homepageNewsletter: HomepageNewsletter? = null,

	@field:SerializedName("homepage-products")
	val homepageProducts: HomepageProducts? = null,

	@field:SerializedName("homepage-slider")
	val homepageSlider: HomepageSlider? = null,

	@field:SerializedName("homepage-banner")
	val homepageBanner: HomepageBanner? = null,

	@field:SerializedName("homepage-bestseller")
	val homepageBestseller: HomepageBestseller? = null
)

data class HomepageProducts(

	@field:SerializedName("homepage-products-title-text")
	val homepageProductsTitleText: String? = null
)

data class HomepageBanner(

	@field:SerializedName("homepage-banner-title-text")
	val homepageBannerTitleText: String? = null,

	@field:SerializedName("homepage-banner-bg-img")
	val homepageBannerBgImg: String? = null,

	@field:SerializedName("homepage-banner-vedio-url")
	val homepageBannerVedioUrl: String? = null,

	@field:SerializedName("homepage-banner-heading")
	val homepageBannerHeading: String? = null,

	@field:SerializedName("homepage-banner-sub-text")
	val homepageBannerSubText: String? = null
)

data class HomepageBestseller(

	@field:SerializedName("homepage-bestseller-heading")
	val homepageBestsellerHeading: String? = null
)


data class HomepageSlider(

	@field:SerializedName("homepage-slider-bg-img")
	val homepageSliderBgImg: List<String?>? = null,

	@field:SerializedName("homepage-slider-title-text")
	val homepageSliderTitleText: String? = null,

	@field:SerializedName("homepage-slider-heading")
	val homepageSliderHeading: String? = null,

	@field:SerializedName("homepage-slider-sub-text")
	val homepageSliderSubText: String? = null
)
