package com.workdo.chocolate.utils;

import com.workdo.chocolate.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
