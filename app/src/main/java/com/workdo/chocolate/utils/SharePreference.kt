package com.workdo.chocolate.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

class SharePreference {

    companion object {
        lateinit var mContext: Context
        lateinit var sharedPreferences: SharedPreferences
        val PREF_NAME: String = "Chocolate"
        val PRIVATE_MODE: Int = 0
        lateinit var editor: SharedPreferences.Editor
        const val isLogin: String = "isLogin"
        val token: String = "token"
        val tokenType: String = "tokenType"
        const val userId: String = "userid"
        val userMobile: String = "usermobile"
        val userEmail: String = "useremail"
        val userName: String = "userName"
        val userFirstName: String = "userFirstName"
        val userLastName: String = "userLastName"
        val userProfile: String = "userprofile"
        val currency: String = "currency"
        val currency_name: String = "currency_name"
        val cartCount: String = "cartCount"

        val Coupon_Id: String = "coupon_id"
        val Coupon_Name: String = "coupon_name"
        val Coupon_Code: String = "coupon_code"
        val Coupon_Discount_Type: String = "coupon_discount_type"
        val Coupon_Discount_Number: String = "coupon_discount_number"
        val Coupon_Discount_Amount: String = "coupon_discount_amount"
        val Coupon_Final_Amount: String = "coupon_final_amount"
        val Billing_Company_Name: String = "billing_company_name"
        val Billing_Address: String = "billing_address"
        val Billing_Postecode: String = "billing_postecode"
        val Billing_Country: String = "billing_country"
        val Billing_State: String = "billing_state"
        val Billing_City: String = "billing_city"
        val Delivery_Address: String = "delivery_address"
        val Delivery_Postcode: String = "delivery_postcode"
        val Delivery_Country: String = "delivery_country"
        val Delivery_State: String = "delivery_state"
        val Delivery_City: String = "delivery_city"
        val Payment_Type: String = "payment_type"
        val Payment_Comment: String = "payment_comment"
        val Delivery_Id: String = "delivery_id"
        val Delivery_Comment: String = "delivery_comment"
        val FinalPrice: String = "FinalPrice"

        val GuestCartList="GuestCartList"
        val BillingDetails: String = "BillingDetails"
        val TaxInfo: String = "TaxInfo"
        val GuestCartSubTotal: String = "GuestCartSubTotal"
        val GuestCartTotal: String = "GuestCartTotal"
        val GuestTotal: String = "GuestTotal"
        val PaymentImage: String = "PaymentImage"
        val DeliveryImage: String = "DeliveryImage"
        val DeliveryAddress: String = "DeliveryAddress"
        val GuestCouponData: String = "CouponData"
        val insta: String = "insta"
        val youtube: String = "youtube"
        val messanger: String = "messanger"
        val twitter: String = "twitter"
        val returnPolicy: String = "returnPolicy"
        val Terms: String = "terms"
        val Contact_Us: String = "contact_us"
        val mainid: String = "mainid"
        val subid: String = "subid"
        val clickMenu: String = "clickMenu"
        val stripeSecretKey: String = "stripeSecretKey"
        val stripeKey: String = "stripeKey"
        val homevideo:String = "homevideo"
        const val BaseUrl="BaseUrl"
        const val ImageUrl="ImageUrl"
        const val PaymentUrl="PaymentUrl"
        fun getIntPref(context: Context, key: String): Int {
            val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            return pref.getInt(key, -1)
        }

        fun setIntPref(context: Context, key: String, value: Int) {
            val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            pref.edit().putInt(key, value).apply()
        }

        fun getStringPref(context: Context, key: String): String? {
            val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            return pref.getString(key, "")
        }

        fun setStringPref(context: Context, key: String, value: String) {
            val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            pref.edit().putString(key, value).apply()
        }

        fun getBooleanPref(context: Context, key: String): Boolean {
            val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            return pref.getBoolean(key, false)
        }

        fun setBooleanPref(context: Context, key: String, value: Boolean) {
            val pref: SharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            pref.edit().putBoolean(key, value).apply()
        }

    }

    @SuppressLint("CommitPrefEdits")
    constructor(mContext1: Context) {
        mContext = mContext1
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = sharedPreferences.edit()
    }

    fun mLogout() {
        editor.clear()
        editor.commit()
    }

}