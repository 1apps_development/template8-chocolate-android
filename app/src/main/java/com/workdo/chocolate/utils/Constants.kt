package com.workdo.chocolate.utils

object Constants {
    val Guest="Guest"
    var UserId="userId"
    var Type="type"
    var Services="services"
    var Categories="categories"
    var Providers="providers"
    var ItemClick="ItemClick"
    var ItemEdit="ItemEdit"
    var ItemDelete="ItemDelete"
    var ProductReturn="ProductReturn"
    var FavClick="FavClick"
    var CartClick="CartClick"
    var Id="Id"
    var ProductId="ProductId"
    var IvMinus="ivminus"
    var IvPlus="ivplus"
    var collection="collection_horizontal"
    var dropdown="dropdown"
    const val FLAT="flat"

}