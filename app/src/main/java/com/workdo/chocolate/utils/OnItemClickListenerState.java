package com.workdo.chocolate.utils;

import com.workdo.chocolate.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
