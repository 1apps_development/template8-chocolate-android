package com.workdo.chocolate.utils;

import com.workdo.chocolate.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
