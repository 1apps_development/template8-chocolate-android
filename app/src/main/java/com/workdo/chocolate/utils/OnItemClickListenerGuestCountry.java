package com.workdo.chocolate.utils;

import com.workdo.chocolate.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
