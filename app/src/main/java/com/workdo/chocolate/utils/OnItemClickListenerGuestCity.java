package com.workdo.chocolate.utils;

import com.workdo.chocolate.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
