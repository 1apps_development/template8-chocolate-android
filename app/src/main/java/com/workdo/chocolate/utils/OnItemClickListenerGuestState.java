package com.workdo.chocolate.utils;

import com.workdo.chocolate.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
