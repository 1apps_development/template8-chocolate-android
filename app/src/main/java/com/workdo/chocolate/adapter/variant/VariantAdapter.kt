package com.workdo.chocolate.adapter.variant

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.workdo.chocolate.R
import com.workdo.chocolate.adapter.variant.VariantCollectionList
import com.workdo.chocolate.databinding.CellDropDownBinding
import com.workdo.chocolate.databinding.CellHorizontalCollectionBinding
import com.workdo.chocolate.model.VariantDataItem
import com.workdo.chocolate.model.VariantItem
import com.workdo.chocolate.utils.Constants



class VariantAdapter(
    private val variantList: ArrayList<VariantItem>,
    val context: Activity,
    val defaultVariantName:String,
    private val variantItemClick: (VariantItem) -> Unit
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var collectionAdapter: VariantCollectionList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 1) {
            val view = CellHorizontalCollectionBinding.inflate(LayoutInflater.from(parent.context), parent,false)
            return HorizontalCollectionViewHolder(view)
        }
        val view = CellDropDownBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DropDownViewHolder(view)


    }

    inner class HorizontalCollectionViewHolder(private val itemBinding: CellHorizontalCollectionBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bindItems(
            data: VariantItem,
            context: Activity,
            variantItemClick: (VariantItem) -> Unit
        ) = with(itemBinding)
        {
            itemBinding.tvTitle.text = itemView.context.resources.getString(R.string.select_)
                .plus(" ${data.name.toString()} : ")
            val dataItemList = ArrayList<VariantDataItem>()

            for (i in 0 until data.value?.size!!) {
                dataItemList.add(VariantDataItem(data.value[i], false))
            }

            if (defaultVariantName.isNotEmpty()) {
                val variantName=if(defaultVariantName.contains("-"))
                {
                    defaultVariantName.split("-")[1]
                }else
                {
                    defaultVariantName
                }
                dataItemList.forEach {
                    if (variantName == it.name) {
                        it.isSelect = true
                    }
                }
            }else{
                dataItemList[0].isSelect = true
            }
            data.dataItem = dataItemList

            collectionAdapter = VariantCollectionList(dataItemList, context) {
                variantItemClick(data)
            }
            itemBinding.rvCollection.apply {
                layoutManager = LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
                adapter = collectionAdapter
                isNestedScrollingEnabled = true
            }
        }
    }

    inner class DropDownViewHolder(private val itemBinding: CellDropDownBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        var isCheck = false
        fun bindItems(
            data: VariantItem,
            context: Activity,
            variantItemClick: (VariantItem) -> Unit
        ) = with(itemBinding)
        {
            itemBinding.tvTitle.text = itemView.context.resources.getString(R.string.select_)
                .plus(" ${data.name.toString()} : ")
            val dataItemList = ArrayList<VariantDataItem>()
            for (i in 0 until data.value?.size!!) {
                dataItemList.add(VariantDataItem(data.value[i], false))
            }
            data.dataItem = dataItemList
            val adapter = ArrayAdapter(
                context,
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
                data.value
            )
            itemBinding.spinnerItem.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {

                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        position: Int,
                        p3: Long
                    ) {
                        Log.e("OnItemSelected", Gson().toJson(dataItemList[position]))

                        itemBinding.tvSelectedItem.text=data.value[position]
                        if (isCheck) {

                            for (i in 0 until dataItemList.size) {
                                data.dataItem!![i].isSelect = false
                            }
                            data.dataItem!![position].isSelect = true
                            variantItemClick(data)


                        }else {
                            if (defaultVariantName.isEmpty()) {
                                itemBinding.tvSelectedItem.text = data.value[position]
                                data.dataItem!![position].isSelect = true

                            } else {
                                for (i in 0 until data.value.size) {
                                    if (data.value[i] == defaultVariantName) {
                                        data.dataItem!![i].isSelect = true
                                        itemBinding.spinnerItem.setSelection(i)
                                    }
                                }
                                val variantText= if(defaultVariantName.contains("-"))
                                {
                                    defaultVariantName.split("-")[0]

                                }else
                                {
                                    defaultVariantName
                                }
                                itemBinding.tvSelectedItem.text = variantText

                            }
                        }



                    }
                }


            itemBinding.spinnerItem.adapter = adapter

            itemBinding.tvSelectedItem.setOnClickListener {
                isCheck = true

                itemBinding.spinnerItem.performClick()
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        if (variantList[position].type == Constants.collection) {
            return 1
        } else
            if (variantList[position].type == Constants.dropdown) {
                return 2
            }
        return -1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = variantList[position]
        if (data.type == Constants.collection) {
            (holder as HorizontalCollectionViewHolder).bindItems(data, context, variantItemClick)
        } else if (data.type == Constants.dropdown) {
            (holder as DropDownViewHolder).bindItems(data, context, variantItemClick)

        }

    }

    override fun getItemCount(): Int {
        return variantList.size
    }


}