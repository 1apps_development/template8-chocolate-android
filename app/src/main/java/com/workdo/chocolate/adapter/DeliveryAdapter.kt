package com.workdo.chocolate.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.databinding.CellDeliveryBinding
import com.workdo.chocolate.model.DeliveryData
import com.workdo.chocolate.utils.Constants
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class DeliveryAdapter(
    private val context: Activity,
    private val deliveryList: ArrayList<DeliveryData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<DeliveryAdapter.AddressViewHolder>() {
    var firsttime = 0
    var currency=SharePreference.getStringPref(context,SharePreference.currency).toString()
    inner class AddressViewHolder(private val binding: CellDeliveryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: DeliveryData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until deliveryList.size) {
                    deliveryList[0].isSelect = true
                }
            } else {

            }

            if (data.isSelect == true) {
                binding.ivChecked.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_round_checked,null))
                binding.clMain.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_chocolate_15, null)

            } else {
                binding.ivChecked.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_round_unchecked,null))
                binding.clMain.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.borderwhite_15, null)
            }

            binding.tvDeliveryDate.text=Utils.getDate(data.expeted_delivery.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.imagePath))
                .into(binding.ivDeliveryType)
            binding.tvDeliveryDesc.text = deliveryList[position].description
            binding.tvDeliveryName.text=deliveryList[position].name

            if(data.chargesType=="percentage")
            {
                binding.tvTotalPrice.text=Utils.getPrice(data.amount.toString()).plus("%")
            }else {
                binding.tvTotalPrice.text=currency.plus(Utils.getPrice(data.amount.toString()))
            }

            itemView.setOnClickListener {
                firsttime=1
                deliveryList[0].isSelect = false
                for (element in deliveryList) {
                    element.isSelect = false
                }
                data.isSelect = true
                notifyDataSetChanged()
                itemClick(position, Constants.ItemClick)
            }
            if (data.isSelect == true) {
                SharePreference.setStringPref(context, SharePreference.Delivery_Id, data.id.toString())
                SharePreference.setStringPref(context, SharePreference.DeliveryImage, data.imagePath.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view = CellDeliveryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(deliveryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return deliveryList.size
    }
}