package com.workdo.chocolate.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.databinding.CellCategoryProductBinding
import com.workdo.chocolate.model.FeaturedProductsSub
import com.workdo.chocolate.utils.Constants
import com.workdo.chocolate.utils.ExtensionFunctions.hide
import com.workdo.chocolate.utils.ExtensionFunctions.invisible
import com.workdo.chocolate.utils.ExtensionFunctions.show
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class SearchListAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellCategoryProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(providerList[position], context, position, itemClick)

    }

    override fun getItemCount(): Int {
        return providerList.size
    }

    inner class ViewHolder(private val binding: CellCategoryProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivProduct)

            binding.tvProductName.text = data.name.toString()
            binding.tvProductPrice.text =Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency

           // binding.tvTag.text = data.tagApi
            if (data.inWhishlist == true) {
                binding.ivWishlist.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_like,
                        null
                    )
            } else {
                binding.ivWishlist.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_dislike,
                        null
                    )
            }

            if (data.discountType == "percentage") {
                binding.tvoffers.text = data.discountPrice.toString().plus("%")
            } else {
                binding.tvoffers.text = currency.plus(Utils.getPrice(data.discountPrice.toString()))
            }

            if (Utils.isLogin(context)) {
                binding.ivWishlist.show()
            } else {
                binding.ivWishlist.invisible()
            }

            binding.btnAddtoCart.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            binding.ivWishlist.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

}