package com.workdo.chocolate.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.databinding.CellOrderdetailsBinding
import com.workdo.chocolate.databinding.CellWishlistBinding
import com.workdo.chocolate.model.ProductItem
import com.workdo.chocolate.utils.Constants
import com.workdo.chocolate.utils.ExtensionFunctions.hide
import com.workdo.chocolate.utils.ExtensionFunctions.show
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class OrderDetailsListAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<ProductItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<OrderDetailsListAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellOrderdetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: ProductItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (data.returnStatus == "1") {
                binding.clReturnOrder.hide()
            } else {
                binding.clReturnOrder.show()
            }
            binding.tvWishlist.text = data.name
            binding.tvVariantName.text = data.variantName
            binding.tvPrice.text =
                currency.plus(data.finalPrice.toString().let { Utils.getPrice(it) })
            binding.tvQty.text = context.getString(R.string.qty).plus(" ").plus(data.qty)

            Glide.with(context)
                .load(ApiClient.ImageURL.BASE_URL.plus(data.image)).into(binding.ivWishList)
            binding.clReturnOrder.setOnClickListener {
                itemClick(position, Constants.ProductReturn)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellOrderdetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return orderlist.size
    }
}