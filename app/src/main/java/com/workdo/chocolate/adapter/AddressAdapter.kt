package com.workdo.chocolate.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.chocolate.databinding.CellGetaddressBinding
import com.workdo.chocolate.model.AddressListData
import com.workdo.chocolate.utils.Constants

class AddressAdapter(
    private val context: Activity,
    private val addressList: ArrayList<AddressListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<AddressAdapter.AddressViewHolder>() {

    inner class AddressViewHolder(private val binding: CellGetaddressBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: AddressListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            binding.tvAddressType.text = addressList[position].title
            binding.tvAddress.text = addressList[position].address.plus(", ").plus(
                addressList[position].city.plus(", ").plus(addressList[position].stateName).plus(", ")
                    .plus(addressList[position].countryName).plus("").plus(" - ")
                    .plus(addressList[position].postcode)
            )
            binding.tvEdit.setOnClickListener {
                itemClick(position,Constants.ItemEdit)
            }
            binding.tvDelete.setOnClickListener {
                itemClick(position,Constants.ItemDelete)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view =
            CellGetaddressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(addressList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return addressList.size
    }
}