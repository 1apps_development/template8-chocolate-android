package com.workdo.chocolate.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.databinding.CellLoyalityBinding
import com.workdo.chocolate.model.OrderListData
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class LoyalitylistAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<OrderListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<LoyalitylistAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellLoyalityBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: OrderListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvDate.text =
                context.getString(R.string.date).plus(" ").plus(Utils.getDate(data.date.toString()))

            binding.tvOrderId.text = "#".plus(data.productOrderId)

            binding.tvFeaturedProductPrice.text ="+".plus(" ").plus(Utils.getPrice(data.rewardPoints.toString())).plus(" ").plus(
                currency)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellLoyalityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {
        return orderlist.size
    }
}