package com.workdo.chocolate.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.databinding.CellCategoriesBinding
import com.workdo.chocolate.databinding.CellCategoriesProductBinding
import com.workdo.chocolate.model.HomeCategoriesItem
import com.workdo.chocolate.utils.Constants

class CategoriesProductAdapter(
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategoriesProductAdapter.AddressViewHolder>() {
    var firsttime = 0
    inner class AddressViewHolder(private val binding: CellCategoriesProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until categoryList.size) {
                    categoryList[0].isSelect = true
                    binding.card.background =
                        ResourcesCompat.getDrawable(context.resources, R.drawable.border_chocolate_15, null)
                }
            } else {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_black_border_white, null)
            }

            if (data.isSelect) {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.border_chocolate_15, null)

            } else {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_black_border_white, null)
            }
            binding.tvCategoriesName.text = data.name
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.image)).into(binding.ivCategories)

            itemView.setOnClickListener {
                firsttime=1
                categoryList[0].isSelect = false
                for (element in categoryList) {
                    element.isSelect = false
                }
                data.isSelect = true
                notifyDataSetChanged()

                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view =
            CellCategoriesProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return categoryList.size
    }
}