package com.workdo.chocolate.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.chocolate.databinding.CellTaxBinding
import com.workdo.chocolate.model.TaxItem
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class TaxListAdapter(
    private val context: Activity,
    private val taxlist: ArrayList<TaxItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<TaxListAdapter.TaxViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class TaxViewHolder(private val binding: CellTaxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: TaxItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvVatLabel.text = data.taxString
            binding.tvVat.text = currency.plus(Utils.getPrice(data.amountstring.toString()))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxViewHolder {
        val view =
            CellTaxBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TaxViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaxViewHolder, position: Int) {
        holder.bind(taxlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return taxlist.size
    }
}