package com.workdo.chocolate.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.model.CategoryDataItem

class CategoriesAdapter(
    var context: Activity,
    private val featuredList: ArrayList<CategoryDataItem>,
    private val onClick: (String, String,String,String) -> Unit
) : RecyclerView.Adapter<CategoriesAdapter.viewHolder>() {

    var firsttime = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_categorieshome, parent, false)

        return viewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val categoriesModel = featuredList[position]

        if (firsttime == 0) {
            for (i in 0 until featuredList.size) {
                featuredList[0].isSelect = true
                holder.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_white_42, null)
                holder.textView.setTextColor(getColor(context, R.color.chocolate))
            }
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_chocolate_42, null)
            holder.textView.setTextColor(getColor(context, R.color.white))
        }

        if (categoriesModel.isSelect == true) {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.bg_white_42, null)
            holder.textView.setTextColor(getColor(context, R.color.chocolate))
        } else {
            holder.card.background =
                ResourcesCompat.getDrawable(context.resources, R.drawable.border_chocolate_42, null)
            holder.textView.setTextColor(getColor(context, R.color.white))
        }

        holder.textView.text = categoriesModel.name
        Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(categoriesModel.iconPath)).into(holder.imageView)

        holder.itemView.setOnClickListener {
            firsttime=1
            featuredList[0].isSelect = false
            for (element in featuredList) {
                element.isSelect = false
            }
            categoriesModel.isSelect = true
            notifyDataSetChanged()

            onClick(
                categoriesModel.id.toString(),
                categoriesModel.name.toString(),
                categoriesModel.maincategoryId.toString(),
                categoriesModel.iconPath.toString()
            )
        }
    }

    override fun getItemCount(): Int {
        return featuredList.size
    }

    class viewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.tvCategoriesname)
        val imageView: ImageView = itemView.findViewById(R.id.ivCategoriesicon)
        val card: ConstraintLayout = itemView.findViewById(R.id.cl)
    }

    private fun onClick(id: String, name: String, mainId: String,iconPath:String) {
        onClick.invoke(id, name, mainId,iconPath)
    }
}