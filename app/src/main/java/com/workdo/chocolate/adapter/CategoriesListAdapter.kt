package com.workdo.chocolate.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.databinding.CellProductlistingBinding
import com.workdo.chocolate.model.HomeCategoriesItem

import com.workdo.chocolate.utils.Constants

class CategoriesListAdapter(
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategoriesListAdapter.AddressViewHolder>() {
    var firsttime = 0
    inner class AddressViewHolder(private val binding: CellProductlistingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until categoryList.size) {
                    categoryList[0].isSelect = true
                }
            }
            binding.ivCart.imageTintList= ColorStateList.valueOf(ResourcesCompat.getColor(itemView.context.resources,R.color.white,null))

            if (data.isSelect) {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_green_11, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.white))
            } else {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_white_11, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.white))
            }
            binding.tvProductName.text = data.name
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.iconPath)).into(binding.ivCart)

            itemView.setOnClickListener {
                firsttime=1
                categoryList[0].isSelect = false
                for (element in categoryList) {
                    element.isSelect = false
                }
                data.isSelect = true
                notifyDataSetChanged()

                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view = CellProductlistingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return categoryList.size
    }
}