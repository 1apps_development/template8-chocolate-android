package com.workdo.chocolate.ui.authentication

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActLoginBinding
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.activity.MainActivity
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActLogin : BaseActivity() {
    private lateinit var _binding: ActLoginBinding
    var token = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActLoginBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        FirebaseApp.initializeApp(this@ActLogin)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    println("Failed to get token")
                    return@OnCompleteListener
                }
                token = task.result
                Log.d("Token-->", token)
            })
        _binding.tvSignup.setOnClickListener { openActivity(ActRegisterOption::class.java) }
        _binding.tvForgotPassword.setOnClickListener { openActivity(ActForgotPassword::class.java) }
        _binding.btnLogin.setOnClickListener {
            when {
                _binding.edEmailAddress.text?.isEmpty() == true -> {
                    Utils.errorAlert(this@ActLogin, resources.getString(R.string.validation_email))
                }
                !Utils.isValidEmail(_binding.edEmailAddress.text.toString()) -> {
                    Utils.errorAlert(
                        this@ActLogin,
                        resources.getString(R.string.validation_valid_email)
                    )
                }
                _binding.edPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(this@ActLogin, resources.getString(R.string.validation_password_))
                }
                else -> {
                    val loginRequest = HashMap<String, String>()
                    loginRequest["email"] = _binding.edEmailAddress.text.toString()
                    loginRequest["password"] = _binding.edPassword.text.toString()
                    loginRequest["token"] = token
                    loginRequest["device_type"] = "android"
                    loginRequest["theme_id"]=getString(R.string.theme_id)
                    callLoginApi(loginRequest)
                }
            }
        }
    }


    private fun emptyGuestData()
    {
        SharePreference.setStringPref(this@ActLogin,SharePreference.GuestCartTotal,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.GuestCartList,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.GuestCartSubTotal,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.PaymentImage,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.DeliveryImage,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.GuestCouponData,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.TaxInfo,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.BillingDetails,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.Payment_Type,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.Payment_Comment,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.Delivery_Id,"")
        SharePreference.setStringPref(this@ActLogin,SharePreference.Delivery_Comment,"")
    }

    //TODO Login api calling
    private fun callLoginApi(loginRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActLogin)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLogin).getLogin(loginRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loginResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            emptyGuestData()
                            Log.e("UserId", loginResponse?.id.toString())
                            SharePreference.setBooleanPref(
                                this@ActLogin,
                                SharePreference.isLogin,
                                true
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userId,
                                loginResponse?.id.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userName,
                                loginResponse?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userFirstName,
                                loginResponse?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userLastName,
                                loginResponse?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userEmail,
                                loginResponse?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userProfile,
                                loginResponse?.image.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.userMobile,
                                loginResponse?.mobile.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.token,
                                loginResponse?.token.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLogin,
                                SharePreference.tokenType,
                                loginResponse?.tokenType.toString()
                            )
                            startActivity(
                                Intent(
                                    this@ActLogin,
                                    MainActivity::class.java
                                )
                            )
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(this@ActLogin, loginResponse?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(this@ActLogin, loginResponse?.message.toString())
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActLogin)
                    }else{
                        Utils.errorAlert(
                            this@ActLogin,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLogin,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLogin,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}