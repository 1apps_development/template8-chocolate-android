package com.workdo.chocolate.ui.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.chocolate.R
import com.workdo.chocolate.adapter.LoyalitylistAdapter
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActLoyalityBinding
import com.workdo.chocolate.model.OrderListData
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.utils.ExtensionFunctions.hide
import com.workdo.chocolate.utils.ExtensionFunctions.show
import com.workdo.chocolate.utils.PaginationScrollListener
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActLoyality : BaseActivity() {
    private lateinit var _binding: ActLoyalityBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<OrderListData>()
    private lateinit var loyalitylistAdapter: LoyalitylistAdapter
    private var manager: LinearLayoutManager? = null


    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActLoyalityBinding.inflate(layoutInflater)
        init()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        manager = LinearLayoutManager(this@ActLoyality)
        orderListAdapter(orderList)
        pagination()
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnCopyURl.setOnClickListener {
            dlgCopyOrderNumber(_binding.tvURL.text.toString())

        }

        loyalityProgram()
    }

    private fun dlgCopyOrderNumber(message: String?) {
        val builder = AlertDialog.Builder(this@ActLoyality)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.url_copied)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Url_copied", message)
            clipboard.setPrimaryClip(clip)
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callOrderList()
            }
        }
        _binding.rvLoyality.addOnScrollListener(paginationListener)
    }

    private fun loyalityProgram() {
        Utils.showLoadingProgress(this@ActLoyality)
        val hashmap = HashMap<String,String>()
        hashmap["theme_id"]= getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyality)
                .loyalityProgramJson(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loyalityProgramResponse = response.body.data?.loyalityProgram
                    when (response.body.status) {
                        1 -> {
                            _binding.tvProgramTitle.text =
                                loyalityProgramResponse?.loyalityProgramTitle
                            _binding.tvProgramDesc.text =
                                loyalityProgramResponse?.loyalityProgramDescription

                            _binding.tvYourFriend.text =
                                loyalityProgramResponse?.loyalityProgramCopyThisLinkAndSendToYourFriends
                            loyalityReward(loyalityProgramResponse?.loyalityProgramYourCash)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyality,
                                loyalityProgramResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyality,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActLoyality)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyality,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyality,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyality,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loyalityReward(loyalityProgramYourCash: String?) {
        val loyalityReward = java.util.HashMap<String, String>()
        loyalityReward["user_id"] =
            SharePreference.getStringPref(this@ActLoyality, SharePreference.userId)
                .toString()
        loyalityReward["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyality)
                .loyalityReward(loyalityReward)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loyalityRewardResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            _binding.tvProgramYourCash.text =
                                loyalityProgramYourCash?.plus(": ").plus("+")
                                    .plus(loyalityRewardResponse?.point).plus(
                                        SharePreference.getStringPref(
                                            this@ActLoyality,
                                            SharePreference.currency_name
                                        )
                                    ).toString()
                            _binding.tvURL.text = ApiClient.ImageURL.BASE_URL.toString()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyality,
                                loyalityRewardResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyality,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActLoyality)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyality,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyality,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyality,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun callOrderList() {
        val categoriesProduct = java.util.HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(this@ActLoyality, SharePreference.userId).toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyality)
                .getOrderList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val OrderListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvLoyality.show()
                                currentPage =
                                    OrderListResponse?.currentPage!!.toInt()
                                total_pages =
                                    OrderListResponse.lastPage!!.toInt()
                                OrderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvLoyality.hide()
                            }
                            loyalitylistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyality,
                                OrderListResponse?.data?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyality,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActLoyality)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyality,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyality,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyality,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        callOrderList()
    }


    private fun orderListAdapter(orderHistorylist: ArrayList<OrderListData>) {
        _binding.rvLoyality.layoutManager = manager
        loyalitylistAdapter =
            LoyalitylistAdapter(this@ActLoyality, orderHistorylist) { i: Int, s: String ->

            }
        _binding.rvLoyality.adapter = loyalitylistAdapter
    }

}