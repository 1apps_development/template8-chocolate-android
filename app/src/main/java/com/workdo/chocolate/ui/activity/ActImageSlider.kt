package com.workdo.chocolate.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.denzcoskun.imageslider.models.SlideModel
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActImageSliderBinding
import com.workdo.chocolate.model.ProductImageItem

class ActImageSlider : BaseActivity() {
    private lateinit var _binding:ActImageSliderBinding
    var imgList: ArrayList<ProductImageItem>? = null
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActImageSliderBinding.inflate(layoutInflater)
        imgList = intent.getParcelableArrayListExtra("imageList")
        Log.e("ImageList",imgList.toString())
        val imageList = ArrayList<SlideModel>()
        for (i in 0 until imgList?.size!!) {
            val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(imgList!![i].imagePath))
            imageList.add(slideModel)
        }
        _binding.imageSlider.setImageList(imageList)
        _binding.ivCancle.setOnClickListener {
            finish()
        }
    }

}