package com.workdo.chocolate.ui.authentication

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActAuthenticationBinding
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActAuthentication : BaseActivity() {
    private lateinit var _binding: ActAuthenticationBinding
    var strEmail: String = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActAuthenticationBinding.inflate(layoutInflater)
        strEmail = intent.getStringExtra("email") ?: ""
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnSendCode.setOnClickListener {
            if (_binding.edOTP.text.toString().isEmpty()) {
                Utils.errorAlert(
                    this@ActAuthentication,
                    resources.getString(R.string.validation_otp)
                )
            } else {
                val forgotpasswordverifyotp = HashMap<String, String>()
                forgotpasswordverifyotp["email"] = strEmail.toString()
                forgotpasswordverifyotp["otp"] = _binding.edOTP.text.toString()
                forgotpasswordverifyotp["theme_id"]=getString(R.string.theme_id)
                callForgotPasswordVerifyOpt(forgotpasswordverifyotp)
            }
        }
        _binding.tvContactUs.setOnClickListener {
            val contactUs=
                SharePreference.getStringPref(this@ActAuthentication,SharePreference.Contact_Us).toString()
            val uri: Uri =
                Uri.parse(contactUs)

            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    //TODO OTP verify api
    private fun callForgotPasswordVerifyOpt(forgotpasswordverifyotp: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActAuthentication)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAuthentication)
                .setforgotpasswordverifyotp(forgotpasswordverifyotp)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val forgotPasswordVerifyOtpResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActAuthentication,
                                    ActForgotPasswordSave::class.java
                                ).putExtra(
                                    "email",
                                    strEmail.toString()
                                )
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActAuthentication,
                                forgotPasswordVerifyOtpResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAuthentication,
                                forgotPasswordVerifyOtpResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActAuthentication)
                    }else{
                        Utils.errorAlert(
                            this@ActAuthentication,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAuthentication,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAuthentication,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}