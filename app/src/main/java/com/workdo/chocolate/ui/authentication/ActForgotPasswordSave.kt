package com.workdo.chocolate.ui.authentication

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActForgotPasswordSaveBinding
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActForgotPasswordSave : BaseActivity() {
    private lateinit var _binding:ActForgotPasswordSaveBinding
    var strEmail: String = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding=ActForgotPasswordSaveBinding.inflate(layoutInflater)
        strEmail = intent.getStringExtra("email") ?: ""
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnSubmit.setOnClickListener {
            when {
                _binding.edNewPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        resources.getString(R.string.validation_password_)
                    )
                }
                _binding.edConfirmPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        resources.getString(R.string.validation_confirm_password)
                    )
                }
                _binding.edNewPassword.text.toString() != _binding.edConfirmPassword.text.toString() -> {
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        resources.getString(R.string.validation_valid_password)
                    )
                }
                else -> {
                    val forgotPasswordSave = HashMap<String, String>()
                    forgotPasswordSave["email"] = strEmail.toString()
                    forgotPasswordSave["password"] = _binding.edNewPassword.text.toString()
                    forgotPasswordSave["theme_id"] = getString(R.string.theme_id)
                    callForgotPasswordSaveApi(forgotPasswordSave)
                }
            }
        }
        _binding.tvContactUs.setOnClickListener {
            val contactUs =
                SharePreference.getStringPref(
                    this@ActForgotPasswordSave,
                    SharePreference.Contact_Us
                ).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    //TODO change password api
    private fun callForgotPasswordSaveApi(forgotPasswordSave: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActForgotPasswordSave)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActForgotPasswordSave)
                .setforgotpasswordsave(forgotPasswordSave)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val forgotpasswordsave = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActForgotPasswordSave,
                                    ActLogin::class.java
                                )
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActForgotPasswordSave,
                                forgotpasswordsave?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActForgotPasswordSave,
                                forgotpasswordsave?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActForgotPasswordSave)
                    } else {
                        Utils.errorAlert(
                            this@ActForgotPasswordSave,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        "Something went wrong"
                    )
                }
            }
        }
    }

}