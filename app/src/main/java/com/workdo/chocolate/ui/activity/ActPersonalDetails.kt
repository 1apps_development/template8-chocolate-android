package com.workdo.chocolate.ui.activity

import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActPersonalDetailsBinding
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActPersonalDetails : BaseActivity() {
    private lateinit var _binding: ActPersonalDetailsBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActPersonalDetailsBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.edFirstName.setText(
            SharePreference.getStringPref(this@ActPersonalDetails,
                SharePreference.userFirstName)
                .toString()
        )
        _binding.edLastName.setText(
            SharePreference.getStringPref(
                this@ActPersonalDetails,
                SharePreference.userLastName
            ).toString()
        )
        _binding.edEmail.setText(
            SharePreference.getStringPref(
                this@ActPersonalDetails,
                SharePreference.userEmail
            ).toString()
        )
        _binding.edPhone.setText(
            SharePreference.getStringPref(
                this@ActPersonalDetails,
                SharePreference.userMobile
            ).toString()
        )

        _binding.btnEditProfile.setOnClickListener {
            if (_binding.edFirstName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_f_name))
            } else if (_binding.edLastName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_l_name))
            } else if (_binding.edEmail.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_email))
            } else if (!Utils.isValidEmail(_binding.edEmail.text.toString())) {
                Utils.errorAlert(
                    this,
                    resources.getString(R.string.validation_valid_email)
                )
            } else if (_binding.edPhone.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_phone_number))
            } else {
                val editprofile = HashMap<String, String>()
                editprofile["user_id"] =
                    SharePreference.getStringPref(this@ActPersonalDetails, SharePreference.userId)
                        .toString()
                editprofile["first_name"] = _binding.edFirstName.text.toString()
                editprofile["last_name"] = _binding.edLastName.text.toString()
                editprofile["email"] = _binding.edEmail.text.toString()
                editprofile["telephone"] = _binding.edPhone.text.toString()
                editprofile["theme_id"] = getString(R.string.theme_id)
                callEditProfile(editprofile)
            }
        }
    }

    private fun callEditProfile(editprofile: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActPersonalDetails)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActPersonalDetails).setProfileUpdate(editprofile)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val editProfileResponse = response.body.data?.data
                    when (response.body.status) {
                        1 -> {
                            Log.e("UserData", editProfileResponse.toString())
                            SharePreference.setStringPref(
                                this@ActPersonalDetails,
                                SharePreference.userName,
                                editProfileResponse?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetails,
                                SharePreference.userFirstName,
                                editProfileResponse?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetails,
                                SharePreference.userLastName,
                                editProfileResponse?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetails,
                                SharePreference.userEmail,
                                editProfileResponse?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetails,
                                SharePreference.userMobile,
                                editProfileResponse?.mobile.toString()
                            )
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActPersonalDetails,
                                response.body.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPersonalDetails,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActPersonalDetails)
                    } else {
                        Utils.errorAlert(
                            this@ActPersonalDetails,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPersonalDetails,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPersonalDetails,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}