package com.workdo.chocolate.ui.activity

import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.chocolate.R
import com.workdo.chocolate.adapter.DeliveryAdapter
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActDeliveryBinding
import com.workdo.chocolate.model.DeliveryData
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.ui.option.ActCart
import com.workdo.chocolate.utils.ExtensionFunctions.hide
import com.workdo.chocolate.utils.ExtensionFunctions.show
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActDelivery : BaseActivity() {
    private lateinit var _binding: ActDeliveryBinding
    private var deliveryList = ArrayList<DeliveryData>()
    private lateinit var deliveryAdapter: DeliveryAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""


    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActDeliveryBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }

        _binding.btnContinue.setOnClickListener {
            comment = _binding.edNote.text.toString()
            SharePreference.setStringPref(
                this@ActDelivery,
                SharePreference.Delivery_Comment,
                comment.toString()
            )
            openActivity(ActPayment::class.java)

        }
        _binding.clcart.setOnClickListener { openActivity(ActCart::class.java) }
        manager = GridLayoutManager(this@ActDelivery, 1, GridLayoutManager.VERTICAL, false)
    }

    //TODO delivery list api
    private fun callDeliveryList() {
        Utils.showLoadingProgress(this@ActDelivery)
        lifecycleScope.launch {
            val request=HashMap<String,String>()
            request["theme_id"]=resources.getString(R.string.theme_id)
            when (val response = ApiClient.getClient(this@ActDelivery)
                .deliveryList(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvDelivery.show()
                                _binding.vieww.hide()
                                paymentListResponse.data?.let {
                                    deliveryList.addAll(it)
                                }
                            } else {
                                _binding.rvDelivery.hide()
                               _binding.vieww.show()
                            }
                            deliveryAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActDelivery,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActDelivery,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActDelivery)
                    } else {
                        Utils.errorAlert(
                            this@ActDelivery,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActDelivery,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActDelivery,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO delivery list adapter
    private fun DeliveryListAdapter(deliveryList: ArrayList<DeliveryData>) {
        _binding.rvDelivery.layoutManager = manager
        deliveryAdapter =
            DeliveryAdapter(this@ActDelivery, deliveryList) { i: Int, s: String ->
                if (deliveryList[i].isSelect == true) {
                    SharePreference.setStringPref(
                        this@ActDelivery,
                        SharePreference.Delivery_Id,
                        deliveryList[i].id.toString()
                    )

                    SharePreference.setStringPref(
                        this@ActDelivery,
                        SharePreference.DeliveryImage,
                        deliveryList[i].imagePath.toString()
                    )
                }
            }
        _binding.rvDelivery.adapter = deliveryAdapter
    }

    override fun onResume() {
        super.onResume()
        deliveryList.clear()
        DeliveryListAdapter(deliveryList)
        callDeliveryList()
    }

}