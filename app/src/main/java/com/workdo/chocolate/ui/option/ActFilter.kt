package com.workdo.chocolate.ui.option

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.workdo.chocolate.base.BaseActivity
import com.mohammedalaa.seekbar.DoubleValueSeekBarView
import com.mohammedalaa.seekbar.OnDoubleValueSeekBarChangeListener
import com.workdo.chocolate.R
import com.workdo.chocolate.adapter.TagListAdapter
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.databinding.ActFilterBinding
import com.workdo.chocolate.model.CategorylistData
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.utils.ExtensionFunctions.hide
import com.workdo.chocolate.utils.ExtensionFunctions.show
import com.workdo.chocolate.utils.ItemClick
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch


class ActFilter : BaseActivity() , ItemClick {
    private lateinit var _binding: ActFilterBinding
    private var categoriesList = ArrayList<CategorylistData>()
    private lateinit var tagAdapter: TagListAdapter
    var ratting = ""
    var name: String = ""
    var id: String = ""
    var minValue: String = "0"
    var maxValue: String = "0"
    var currency = ""

    private var manager: GridLayoutManager? = null

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActFilterBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {

        currency = SharePreference.getStringPref(this@ActFilter, SharePreference.currency_name).toString()
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnDelete.setOnClickListener { finish() }
        _binding.btnFilter.setOnClickListener {
            if (id != "") {
                val intent = Intent(this@ActFilter, ActSearch::class.java)
                intent.putExtra("name", name.toString())
                intent.putExtra("id", id.toString())
                intent.putExtra("ratting", ratting.toString())
                intent.putExtra("minValue", minValue.toString())
                intent.putExtra("maxValue", maxValue.toString())
                intent.putExtra("filter", "filter")
                Log.e("IntentCall",id+ratting+minValue+maxValue)
                setResult(500, intent)
                finish()
            } else {
                Utils.errorAlert(
                    this@ActFilter,
                    resources.getString(R.string.validation_alert)
                )
            }
        }
        manager = GridLayoutManager(this@ActFilter, 3, GridLayoutManager.VERTICAL, false)

        _binding.doubleRangeSeekbar.setOnRangeSeekBarViewChangeListener(object :
            OnDoubleValueSeekBarChangeListener {
            override fun onValueChanged(
                seekBar: DoubleValueSeekBarView?,
                min: Int,
                max: Int,
                fromUser: Boolean
            ) {
                Log.e("onChanged->", "Min $min : Max $max")
                _binding.tvminpricevalue.text = min.toString().plus("").plus("USD")
                _binding.tvmxnpricevalue.text = max.toString().plus("").plus("USD")
            }

            override fun onStartTrackingTouch(
                seekBar: DoubleValueSeekBarView?,
                min: Int,
                max: Int
            ) {
                Log.e("onStart->", "Min $min : Max $max")
                _binding.tvminpricevalue.text = min.toString().plus("").plus("USD")
                _binding.tvmxnpricevalue.text = max.toString().plus("").plus("USD")
                minValue = min.toString()
                maxValue = max.toString()
            }

            override fun onStopTrackingTouch(seekBar: DoubleValueSeekBarView?, min: Int, max: Int) {
                Log.e("onStop->", "Min $min : Max $max")
                _binding.tvminpricevalue.text = min.toString().plus("").plus(" USD")
                _binding.tvmxnpricevalue.text = max.toString().plus("").plus(" USD")
                minValue = min.toString()
                maxValue = max.toString()
            }
        })

        _binding.cl1star.setOnClickListener {
            _binding.iv1check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_checked,
                    null
                )
            )
            _binding.iv2check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv3check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv4check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv5check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            ratting = "1"
        }
        _binding.cl2star.setOnClickListener {
            _binding.iv1check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv2check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_checked,
                    null
                )
            )
            _binding.iv3check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv4check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv5check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            ratting = "2"
        }
        _binding.cl3star.setOnClickListener {
            _binding.iv1check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv2check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv3check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_checked,
                    null
                )
            )
            _binding.iv4check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv5check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            ratting = "3"
        }
        _binding.cl4star.setOnClickListener {
            _binding.iv1check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv2check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv3check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv4check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_checked,
                    null
                )
            )
            _binding.iv5check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            ratting = "4"
        }
        _binding.cl5star.setOnClickListener {
            _binding.iv1check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv2check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv3check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv4check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_unchecked,
                    null
                )
            )
            _binding.iv5check.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_checked,
                    null
                )
            )
            ratting = "5"
        }
    }

    //TODO Categories api
    private fun callCategories() {
        Utils.showLoadingProgress(this@ActFilter)
        val filtermap = HashMap<String, String>()
        filtermap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActFilter)
                .getCategoryList(filtermap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val featuredProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            _binding.tvmxnpricevalue.text = response.body.maxPrice.toString().plus("").plus(" $currency")
                            maxValue=response.body.maxPrice.toString()
                            _binding.doubleRangeSeekbar.maxValue=response.body.maxPrice?.toInt()?:0
                            response.body.data?.let { categoriesList.addAll(it) }
                            if (categoriesList.size == 0) {
                                _binding.tagview.hide()
                            } else {
                                _binding.tagview.show()
                            }
                            tagAdapter.notifyDataSetChanged()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActFilter,
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActFilter,
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActFilter)
                    } else {
                        Utils.errorAlert(
                            this@ActFilter,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActFilter,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActFilter, "Something went wrong")
                }
            }
        }
    }

    //TODO Catrgorie list set
    private fun CategoriesListAdapter(categoriesList: ArrayList<CategorylistData>) {
        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        _binding.tagview.layoutManager = layoutManager
        tagAdapter =
            TagListAdapter(this@ActFilter, categoriesList, callBack = apply { })
        _binding.tagview.adapter = tagAdapter
    }

    override fun onResume() {
        super.onResume()
        categoriesList.clear()
        CategoriesListAdapter(categoriesList)
        callCategories()
    }


    override fun onClick(s: String, name: ArrayList<String>, id: ArrayList<String>) {
        if (s == "Click") {
            for (i in 0 until name.size) {
                val addons_names = name.toString()
                val addons = addons_names.replace("[", "")
                this.name = addons.replace("]", "")
                Log.e("THISNAME", this.name)
            }
            for (i in 0 until id.size) {
                val addonsId = id.toString()
                val addons = addonsId.replace("[", "")
                this.id = addons.replace("]", "")
                Log.e("THISNAME", this.id)
            }
        }
    }


}