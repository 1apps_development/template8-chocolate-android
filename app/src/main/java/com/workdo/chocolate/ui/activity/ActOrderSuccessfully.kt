package com.workdo.chocolate.ui.activity

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.view.View
import com.workdo.chocolate.R
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActOrderSuccessfullyBinding
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class ActOrderSuccessfully : BaseActivity() {
    private lateinit var _binding: ActOrderSuccessfullyBinding
    var title = ""
    var desc = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActOrderSuccessfullyBinding.inflate(layoutInflater)
        init()
        if (!Utils.isLogin(this@ActOrderSuccessfully)) {
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.GuestCartTotal,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.GuestCartList,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.GuestCartSubTotal,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.PaymentImage,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.DeliveryImage,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.GuestCouponData,
                ""
            )
            SharePreference.setStringPref(this@ActOrderSuccessfully, SharePreference.TaxInfo, "")
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.BillingDetails,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Payment_Type,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Payment_Comment,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Id,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Comment,
                ""
            )
            SharePreference.setStringPref(this@ActOrderSuccessfully, SharePreference.cartCount, "")

        }
        else {
            SharePreference.setStringPref(this@ActOrderSuccessfully, SharePreference.Coupon_Id, "")
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Coupon_Name,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Coupon_Code,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Coupon_Discount_Type,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Coupon_Discount_Number,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Coupon_Discount_Amount,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Coupon_Final_Amount,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Billing_Company_Name,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Billing_Address,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Billing_Postecode,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Billing_Country,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Billing_State,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Billing_City,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Address,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Postcode,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Country,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_State,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_City,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Payment_Type,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Payment_Comment,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Id,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderSuccessfully,
                SharePreference.Delivery_Comment,
                ""
            )
            SharePreference.setStringPref(this@ActOrderSuccessfully, SharePreference.cartCount, "")

        }
    }

    private fun init() {
        _binding.btnBackToHome.setOnClickListener {

            openActivity(MainActivity::class.java)
        }
        title = intent.getStringExtra("orderTitle").toString()
        desc = intent.getStringExtra("desc").toString()
        val ordernumber = title.split("#")
        _binding.tvOrderNumber.text = "#".plus(ordernumber[1])
        _binding.tvyouorder.text = ordernumber[0]
        _binding.tvyouordersuccess.text = desc

        _binding.tvOrderNumber.setOnClickListener {
            dlgCopyOrderNumber(_binding.tvOrderNumber.text.toString())
        }
    }

    private fun dlgCopyOrderNumber(message: String?) {
        val builder = AlertDialog.Builder(this@ActOrderSuccessfully)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.order_number_copied)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("PhoneNumber",message)
            clipboard.setPrimaryClip(clip)
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        openActivity(MainActivity::class.java)
    }
}
