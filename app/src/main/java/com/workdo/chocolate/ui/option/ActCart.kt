package com.workdo.chocolate.ui.option

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActCartBinding

class ActCart : BaseActivity() {
    private lateinit var _binding: ActCartBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActCartBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {

    }
}