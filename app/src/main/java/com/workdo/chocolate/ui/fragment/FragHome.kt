package com.workdo.chocolate.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.chocolate.R
import com.workdo.chocolate.adapter.BestsellerAdapter
import com.workdo.chocolate.adapter.CategoriesAdapter
import com.workdo.chocolate.adapter.FeaturedAdapter
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseFragment
import com.workdo.chocolate.databinding.DlgConfirmBinding
import com.workdo.chocolate.databinding.FragHomeBinding
import com.workdo.chocolate.model.CategoryDataItem
import com.workdo.chocolate.model.FeaturedProductsSub
import com.workdo.chocolate.model.ProductListItem
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.activity.ActProductDetails
import com.workdo.chocolate.ui.activity.ActShoppingCart
import com.workdo.chocolate.ui.activity.MainActivity
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.ui.option.ActCart
import com.workdo.chocolate.ui.option.ActMenu
import com.workdo.chocolate.ui.option.ActSearch
import com.workdo.chocolate.utils.Constants
import com.workdo.chocolate.utils.ExtensionFunctions.hide
import com.workdo.chocolate.utils.ExtensionFunctions.show
import com.workdo.chocolate.utils.PaginationScrollListener
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragHome : BaseFragment<FragHomeBinding>() {

    private lateinit var _binding: FragHomeBinding
    var count = 1

    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0

    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter

    private var featuredProductsList = ArrayList<CategoryDataItem>()
    private lateinit var featuredAdapter: CategoriesAdapter
    private var manager: GridLayoutManager? = null

    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private lateinit var categoriesProductAdapter: FeaturedAdapter
    private var categoriesProductmanager: GridLayoutManager? = null

    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var total_pagesBestSeller: Int = 0

    var subcategory_id: String = ""
    var maincategory_id: String = ""
    var searched = ""
    var currency = ""
    var currency_name = ""

    override fun initView(view: View) {
        _binding = FragHomeBinding.bind(_binding.root)
        init()
    }

    override fun getBinding(): FragHomeBinding {
        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {

        _binding.clSearch.setOnClickListener { openActivity(ActSearch::class.java) }
        _binding.edSearch.setOnClickListener { openActivity(ActSearch::class.java) }

        _binding.btnCheckMore.setOnClickListener {
            val intent = Intent(requireActivity(), MainActivity::class.java)
            intent.putExtra("pos", "2")
            startActivity(intent)
        }

        _binding.btnplayoff.setOnClickListener{
            val contact =
                SharePreference.getStringPref(requireActivity(),SharePreference.homevideo).toString()
            val uri:Uri =
                Uri.parse(contact)
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
        }

        managerBestsellers =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        manager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        categoriesProductmanager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        featuredProductsAdapter()
        paginationBestSeller()
        pagination()

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }

        _binding.clcart.setOnClickListener {
            openActivity(ActShoppingCart::class.java)
        }
        _binding.ivMenu.setOnClickListener { openActivity(ActMenu::class.java) }
    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(categoriesProductmanager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callCategorysProduct(true)
            }
        }
        _binding.rvProductsecond.addOnScrollListener(paginationListener)
    }

    //TODO Currency Api calling
    private fun callCurrencyApi() {
        Utils.showLoadingProgress(requireActivity())
        var currencyMap = HashMap<String, String>()
        currencyMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setcurrency(currencyMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val currencyResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency,
                                currencyResponse?.currency.toString()
                            )
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency_name,
                                currencyResponse?.currency_name.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                currencyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val theme = HashMap<String, String>()
        theme["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(theme)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //homepage-header
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepageHeader?.homepageHeaderBgImg))
                                .into(_binding.ivHomeBg)
                            _binding.tvchocolate.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderHeading
                            _binding.tvchocolatedesc.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderTitleText
                            _binding.tvdesc.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderSubText
                            _binding.btnCheckMore.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeader

                            //homepage-slider
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepageSlider?.homepageSliderBgImg?.get(0).toString()))
                                .into(_binding.ivHomeSuger)
                            _binding.tvchocolate1.text =
                                headerContenResponse?.themJson?.homepageSlider?.homepageSliderHeading
                            _binding.tvchocolatedesc1.text =
                                headerContenResponse?.themJson?.homepageSlider?.homepageSliderTitleText
                            _binding.tvDesc2.text =
                                headerContenResponse?.themJson?.homepageSlider?.homepageSliderSubText

                            //homepage-products
                            _binding.tvDesc3.text =
                                headerContenResponse?.themJson?.homepageProducts?.homepageProductsTitleText

                            //homepage-banner
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepageBanner?.homepageBannerBgImg))
                                .into(_binding.ivHomechocolate)
                            _binding.tvChocolaterie.text =
                                headerContenResponse?.themJson?.homepageBanner?.homepageBannerHeading
                            _binding.tvDesc4.text =
                                headerContenResponse?.themJson?.homepageBanner?.homepageBannerTitleText
                            _binding.tvDesc5.text =
                                headerContenResponse?.themJson?.homepageBanner?.homepageBannerSubText

                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.homevideo,
                                headerContenResponse?.themJson?.homepageBanner?.homepageBannerVedioUrl.toString()
                            )

                            //homepage-bestseller
                            _binding.tvBestSeller.text =
                                headerContenResponse?.themJson?.homepageBestseller?.homepageBestsellerHeading

                            //homepage-newsletter
                            _binding.tvgetOff.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterTitleText
                            _binding.tvLorem3.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterSubText
                            _binding.tvCondition.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterDescription

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        val bestseller = HashMap<String, String>()
        bestseller["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(currentPageBestSeller.toString(), bestseller)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(currentPageBestSeller.toString(), bestseller)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }
                                _binding.view.hide()
                                _binding.rvProduct.show()
                                /* if (currentPageBestSeller == response.body.data?.lastPage) {
                                     _binding.btnShowMoreBestSellers.hide()
                                 } else {
                                     _binding.btnShowMoreBestSellers.show()
                                 }*/
                                this@FragHome.currentPageBestSeller =
                                    bestsellerResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesBestSeller =
                                    bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }

                                if (currentPageBestSeller >= total_pagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvProduct.hide()
                                _binding.view.show()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvProduct.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvProduct.adapter = bestSellersAdapter
    }

    private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managerBestsellers) {
            override fun isLastPage(): Boolean {
                return isLastPageBestSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingBestSeller
            }

            override fun loadMoreItems() {
                isLoadingBestSeller = true
                currentPageBestSeller++
                callBestseller()
            }
        }
        _binding.rvProduct.addOnScrollListener(paginationListener)
    }

    private fun callFeaturedProduct() {
        Utils.showLoadingProgress(requireActivity())
        val featuredhashmap = HashMap<String, String>()
        featuredhashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(requireActivity())
                    .setcategory(featuredhashmap)) {

                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val featuredProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            response.body.data?.let {
                                featuredProductsList.addAll(it)
                            }
                            if (featuredProductsList.size == 0) {
                                _binding.rvCategories.hide()

                            } else {
                                _binding.rvCategories.show()
                            }
                            maincategory_id=featuredProductsList[0].maincategoryId.toString()
                            featuredAdapter.notifyDataSetChanged()
                            currentPage=1
                            isLastPage=false
                            isLoading=false
                            callCategorysProduct(false)
                            Utils.dismissLoadingProgress()

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                featuredProductResponse?.get(0)?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }

    }

    private fun featuredProductsAdapter() {
        _binding.rvCategories.layoutManager = manager
        featuredAdapter =
            CategoriesAdapter(
                requireActivity(),
                featuredProductsList
            ) { id: String, name: String, mainId: String, iconPath: String ->

                subcategory_id = id.toString()
                maincategory_id = mainId.toString()
                featuredProductsSubList.clear()
                currentPage = 1
                total_pages = 0
                isLastPage = false
                isLoading = false
                callCategorysProduct(true)
            }
        _binding.rvCategories.adapter = featuredAdapter
    }

    private fun callCategorysProduct(fromClick:Boolean) {
        if(fromClick)
        {
            Utils.showLoadingProgress(requireActivity())

        }
        val categoriesProduct = HashMap<String, String>()
//        categoriesProduct["subcategory_id"] = subcategory_id
        categoriesProduct["maincategory_id"] = maincategory_id
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvProductsecond.show()
                                currentPage = categoriesProductResponse?.currentPage!!.toInt()
                                total_pages = categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvProductsecond.hide()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {
        _binding.rvProductsecond.layoutManager = categoriesProductmanager
        categoriesProductAdapter =
            FeaturedAdapter(requireActivity(), featuredProductsSubList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct", i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = featuredProductsSubList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, featuredProductsSubList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvProductsecond.adapter = categoriesProductAdapter
    }


    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = true
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = true
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                }
                            } else {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        featuredProductsSubList[position].inWhishlist = false
                                        categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = false
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            //  dlgConfirm(data.name+" "+R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?,
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            if (response.body.isOutOFStock == 1) {
                                Utils.errorAlert(requireActivity(),
                                    response.body.data?.message.toString())
                            } else {
                                dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                            }

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String,
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }


    override fun onResume() {
        super.onResume()



        lifecycleScope.launch {
            coroutineScope {
                val currency = lifecycleScope.async {
                    callCurrencyApi()
                }
                currency.await()
            }

            callHeaderContentApi()

            currentPageBestSeller = 1
            bestsellersList.clear()
            bestsellerAdapter(bestsellersList)
            callBestseller()

            featuredProductsList.clear()
            featuredProductsAdapter()

            featuredProductsSubList.clear()
            categoriesProductsAdapter(featuredProductsSubList)

            callFeaturedProduct()




            _binding.tvCount.text =
                SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)

        }
    }
}
