package com.workdo.chocolate.ui.activity

import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActChangePasswordBinding
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.ui.option.ActCart
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActChangePassword : BaseActivity() {
    private lateinit var _binding: ActChangePasswordBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActChangePasswordBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }

        _binding.btnChangePassword.setOnClickListener {
            when {
                _binding.edPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_password_)
                    )
                }
                _binding.edConfirmPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_confirm_password)
                    )
                }
                _binding.edPassword.text.toString() != _binding.edConfirmPassword.text.toString() -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_valid_password)
                    )
                }
                else -> {
                    callChangePasswordApi()
                }
            }
        }

    }

    private fun callChangePasswordApi() {
        val request = HashMap<String, String>()
        request["user_id"] =
            SharePreference.getStringPref(this@ActChangePassword, SharePreference.userId).toString()
        request["password"] = _binding.edPassword.text.toString()
        request["theme_id"] =getString(R.string.theme_id)
        Utils.showLoadingProgress(this@ActChangePassword)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActChangePassword).setChangePassword(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val changesPasswordResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActChangePassword,
                                response.body.data?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActChangePassword,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }
                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActChangePassword)
                    } else {
                        Utils.errorAlert(
                            this@ActChangePassword,
                            response.body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.internet_connection_error)
                    )
                }
                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActChangePassword,
                        "Something went wrong"
                    )
                }
            }
        }
    }

}