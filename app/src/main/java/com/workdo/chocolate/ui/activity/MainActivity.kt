package com.workdo.chocolate.ui.activity

import android.app.AlertDialog
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.R
import com.workdo.chocolate.databinding.ActivityMainBinding
import com.workdo.chocolate.ui.authentication.ActWelCome
import com.workdo.chocolate.ui.fragment.*
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils

class MainActivity : BaseActivity() {
    private lateinit var _binding: ActivityMainBinding
    var pos = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActivityMainBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){

        val name = SharePreference.getStringPref(this@MainActivity, SharePreference.userName)
        Log.e("name", name.toString())

        if(SharePreference.getStringPref(this@MainActivity,SharePreference.cartCount).isNullOrEmpty())
        {
            SharePreference.setStringPref(this@MainActivity,SharePreference.cartCount,"0")
        }

        pos = intent.getStringExtra("pos").toString()
        Log.e("Pos", pos)
        if (pos == "1") {
            setCurrentFragment(FragHome())
            _binding.bottomNavigation.selectedItemId = R.id.ivBestSellers
        } else if (pos == "2") {
            setCurrentFragment(FragProduct())
            _binding.bottomNavigation.selectedItemId = R.id.ivProduct
        } else if (pos == "3") {
            setCurrentFragment(FragAllCategories())
            _binding.bottomNavigation.selectedItemId = R.id.ivCategories
        } else if (pos == "4") {
            setCurrentFragment(FragWishList())
            _binding.bottomNavigation.selectedItemId = R.id.ivWishList
        } else if (pos == "5") {
            setCurrentFragment(FragSetting())
            _binding.bottomNavigation.selectedItemId = R.id.ivSettings
        } else {
            setCurrentFragment(FragHome())
        }
        bottomSheetItemNavigation()
    }

    private fun bottomSheetItemNavigation() {
        _binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
            when (item.itemId) {
                R.id.ivBestSellers -> {
                    if (fragment !is FragHome) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragHome())
                    }
                    true
                }
                R.id.ivProduct -> {
                    if (fragment !is FragProduct) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragProduct())
                    }
                    true
                }
                R.id.ivCategories -> {

                    if (fragment !is FragAllCategories) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragAllCategories())
                        SharePreference.setStringPref(this@MainActivity,SharePreference.clickMenu,"click")
                    }else
                    {
                        fragment.showLayout()
                    }
                    true
                }

                R.id.ivWishList -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragWishList) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragWishList())
                        }
                    }else
                    {
                        Utils.openWelcomeScreen(this@MainActivity)
                    }
                    true
                }
                R.id.ivSettings -> {
                    if (Utils.isLogin(this@MainActivity)) {
                        if (fragment !is FragSetting) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragSetting())
                        }
                    } else {
                        Utils.openWelcomeScreen(this@MainActivity)
                    }

                    true
                }
                else -> {
                    false
                }
            }
        }
    }


    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainContainer, fragment)
            commit()
        }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            mExitDialog()
        }
    }

    private fun mExitDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        //set title for alert dialog
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.exit)
        //performing positive action
        builder.setPositiveButton(getString(R.string.yes)) { _, _ ->
            builder.setCancelable(true)
            ActivityCompat.finishAfterTransition(this@MainActivity)
            ActivityCompat.finishAffinity(this@MainActivity);
            finish()
        }
        //performing cancel action
        builder.setNegativeButton(getString(R.string.no)) { _, _ ->
            builder.setCancelable(true)
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}