package com.workdo.chocolate.ui.activity

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.workdo.chocolate.R
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActTrackOrderBinding

class ActTrackOrder : BaseActivity() {
    private lateinit var _binding:ActTrackOrderBinding
    var trackOrderStatus = ""

    override fun setLayout(): View = _binding.root

    @RequiresApi(Build.VERSION_CODES.M)
    override fun initView() {
        _binding= ActTrackOrderBinding.inflate(layoutInflater)
        trackOrderStatus = intent.getStringExtra("trackOrderStatus").toString()
        init()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun init(){
        _binding.ivBack.setOnClickListener { finish() }
        if (trackOrderStatus=="0"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_unoption)
            _binding.view.setBackgroundColor(getColor(R.color.chocolate))
        }else if (trackOrderStatus=="1"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_option)
            _binding.view.setBackgroundColor(getColor(R.color.chocolate))

        }
    }

}