package com.workdo.chocolate.ui.authentication

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.workdo.chocolate.R
import com.workdo.chocolate.api.ApiClient
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActRegisterBinding
import com.workdo.chocolate.remote.NetworkResponse
import com.workdo.chocolate.ui.activity.MainActivity
import com.workdo.chocolate.utils.SharePreference
import com.workdo.chocolate.utils.Utils
import kotlinx.coroutines.launch

class ActRegister : BaseActivity() {
    private lateinit var _binding: ActRegisterBinding
    var token = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActRegisterBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        FirebaseApp.initializeApp(this@ActRegister)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    println("Failed to get token")
                    return@OnCompleteListener
                }
                token = task.result
                Log.d("Token-->", token)
            })

        _binding.tvLogin.setOnClickListener {openActivity(ActLogin::class.java)  }

        _binding.btnregister.setOnClickListener {
            if (_binding.edFirstName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_f_name))
            } else if (_binding.edLastName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_l_name))
            } else if (_binding.edEmailAddress.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_email))
            } else if (!Utils.isValidEmail(_binding.edEmailAddress.text.toString())) {
                Utils.errorAlert(
                    this,
                    resources.getString(R.string.validation_valid_email)
                )
            } else if (_binding.edPhoneNumber.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_phone_number))
            } else if (_binding.edPassword.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_password_))
            } else {
                val signUpRequest = HashMap<String, String>()
                signUpRequest["first_name"] = _binding.edFirstName.text.toString()
                signUpRequest["last_name"] = _binding.edLastName.text.toString()
                signUpRequest["email"] = _binding.edEmailAddress.text.toString()
                signUpRequest["password"] = _binding.edPassword.text.toString()
                signUpRequest["mobile"] = _binding.edPhoneNumber.text.toString()
                signUpRequest["device_type"] = "android"
                signUpRequest["register_type"] = "email"
                signUpRequest["token"] = token
                signUpRequest["theme_id"] = getString(R.string.theme_id)
                callRegisterApi(signUpRequest)
            }
        }

    }

    //TODO register api
    private fun callRegisterApi(signUpRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActRegister)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActRegister).setRegistration(signUpRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val registerResponse = response.body
                    val status = response.body.status
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setBooleanPref(
                                this@ActRegister,
                                SharePreference.isLogin,
                                true
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userId,
                                registerResponse.data?.id.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userName,
                                registerResponse.data?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userFirstName,
                                registerResponse.data?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userProfile,
                                registerResponse.data?.image.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userLastName,
                                registerResponse.data?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userEmail,
                                registerResponse.data?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userMobile,
                                registerResponse.data?.mobile.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.token,
                                registerResponse.data?.token.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.tokenType,
                                registerResponse.data?.tokenType.toString()
                            )
                            startActivity(
                                Intent(
                                    this@ActRegister,
                                    MainActivity::class.java
                                )
                            )
                        }
                        0 -> {
                            Utils.successAlert(
                                this@ActRegister,
                                registerResponse.data?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(this@ActRegister, registerResponse?.message.toString())
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError ->{
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9){
                        Utils.setInvalidToekn(this@ActRegister)
                    }
                    else{
                        Utils.errorAlert(this@ActRegister,response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError->{
                    Utils.errorAlert(
                        this@ActRegister,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError->{
                    Utils.errorAlert(this@ActRegister,"Something went wrong")
                }

            }
        }
    }
}