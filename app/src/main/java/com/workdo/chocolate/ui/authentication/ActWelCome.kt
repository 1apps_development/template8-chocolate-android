package com.workdo.chocolate.ui.authentication

import android.view.View
import com.workdo.chocolate.ui.activity.MainActivity
import com.workdo.chocolate.base.BaseActivity
import com.workdo.chocolate.databinding.ActWelComeBinding

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelComeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelComeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            openActivity(MainActivity::class.java)
        }
    }

}